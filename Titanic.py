import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn import tree


df = pd.read_csv('titanic.csv')
inputs = df.drop(['PassengerId','Name','SibSp','Parch','Ticket','Cabin','Embarked','Survived'], axis='columns')
target = df.Survived
le = LabelEncoder()
inputs['Sex'] = le.fit_transform(inputs.Sex)
X_train, X_test, y_train, y_test = train_test_split(inputs,target,test_size=0.3)
inputs.Age = inputs.Age.fillna(inputs.Age.mean())
print(inputs)
model = tree.DecisionTreeClassifier()
model.fit(X_train,y_train)
print("Model Score = ", model.score(X_test, y_test))
#Model score around 80%