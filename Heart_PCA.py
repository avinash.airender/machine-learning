import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import svm
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler

from sklearn.decomposition import PCA

df = pd.read_csv('heart.csv')
df1 = df[df.Cholesterol<=(df.Cholesterol.mean()+3*df.Cholesterol.std())]
df2 = df1[df1.Oldpeak<=(df1.Oldpeak.mean()+3*df1.Oldpeak.std())]
df3 = df2[df2.RestingBP<=(df2.RestingBP.mean()+3*df2.RestingBP.std())]
df4 = pd.get_dummies(df3, drop_first=True)
X = df4.drop("HeartDisease",axis='columns')
y = df4.HeartDisease


scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)
X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, random_state=30)

model = RandomForestClassifier()
model.fit(X_train, y_train)
print(model.score(X_test, y_test))
#Score = 0.856353591160221

#Reducing Dimensions
pca = PCA(0.95)
X_pca = pca.fit_transform(X)
X_train_pca, X_test_pca, y_train, y_test = train_test_split(X_pca, y, test_size=0.2, random_state=30)
model2 = RandomForestClassifier()
model2.fit(X_train_pca, y_train)
print(model2.score(X_test_pca, y_test))
#Score = 0.712707182320442
