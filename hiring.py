import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
import math

df = pd.read_csv('hiring.csv')
print(df)

#Replacing NaN values with median (data preprocessing)
median = math.floor(df.test_score.median())
df.test_score=df.test_score.fillna(median)
print(df)
reg = linear_model.LinearRegression()
reg.fit(df[['experience','test_score','interview_score(out of 10)']],df.salary)
salary = reg.predict([[2,9,6]])
print('Predicted salary: ', salary)